package com.shekhar.assignment.viewmodel.repository

import android.annotation.SuppressLint
import android.util.Log
import com.shekhar.assignment.viewmodel.repository.api.ApiManager
import com.shekhar.assignment.viewmodel.repository.data.PopularMovieList
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class PopularMovieRepository(val apiManager: ApiManager) {

    fun getPoppularMovies(page_num:Int): Observable<PopularMovieList> {
        return Observable.concatArray(
            getPopularMoviesFromApi(page_num)
        )
    }

    fun getPopularMoviesFromApi(page_num:Int): Observable<PopularMovieList> {
        return apiManager.getPopularMoviesList(page_num.toString())
            .doOnNext {
                Log.d("Dispatching", "Dispatching ${it.results.size} users from API...")
                storeUsersInDb(it)
            }
    }

    @SuppressLint("CheckResult")
    fun storeUsersInDb(movieList: PopularMovieList) {
        Observable.fromCallable { }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe {
                Log.d("Inserted", "Inserted ${movieList.results.size} users from API in DB...")
            }
    }

}
