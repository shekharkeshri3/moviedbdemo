package com.shekhar.assignment.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.shekhar.assignment.R
import com.shekhar.assignment.utils.Constants
import com.shekhar.assignment.viewmodel.repository.data.MovieListItem
import kotlinx.android.synthetic.main.movie_item_layout.view.*

class MovieListAdapter(private val mContext: Context?,
private var list: ArrayList<MovieListItem>) : RecyclerView.Adapter<MovieListAdapter.RecyclerViewHolder>() {

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.movie_item_layout, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindPerformersChild(list.get(position))
    }

    fun setUserList(list: ArrayList<MovieListItem>){
        this.list=list
        notifyDataSetChanged()
    }
    inner class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindPerformersChild(movieListItem: MovieListItem) {
            val requestOptions = RequestOptions()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
            Glide.with(mContext!!)
                .setDefaultRequestOptions(requestOptions)
                .load(Constants.IMAGE_URL+movieListItem.poster_path)
                .into(itemView.mve_banner)

            itemView.setOnClickListener(View.OnClickListener {

            })
        }
    }
}