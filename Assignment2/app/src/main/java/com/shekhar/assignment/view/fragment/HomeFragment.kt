package com.shekhar.assignment.view.fragment

import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.shekhar.assignment.R
import com.shekhar.assignment.base.BaseFragment
import com.shekhar.assignment.view.adapter.HomeListAdapter
import com.shekhar.assignment.view.main.MainActivity
import kotlinx.android.synthetic.main.home_layout.*

class HomeFragment: BaseFragment(), RecyclerClick{

    private var homeList = ArrayList<String>()
    private lateinit var homeListAdapter: HomeListAdapter

    override fun layoutRes(): Int {
        return R.layout.home_layout
    }

    override fun setValues() {
        addList()
        homeListAdapter = HomeListAdapter(context, homeList)
        home_rcv.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 2)
            adapter = this@HomeFragment.homeListAdapter
        }
        homeListAdapter.setOnItemClickListener(this)
    }

    override fun onItemClick(view: View, position: Int) {
        when(view.id){
            R.id.itemCardView -> {
                //open detail page
                (activity as MainActivity).addFragment(HomeDetailFragment.getInstance())
//                Toast.makeText(mContext,homeList.get(position),Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun registerClickListener(): Array<View>? {
        return null
    }

    override fun onClick(v: View?) {
    }
    fun addList(){
        homeList.clear()
        homeList.add("Attitude")
        homeList.add("Beauty")
        homeList.add("Cofee")
        homeList.add("Art")
        homeList.add("Books")
        homeList.add("Birthday")
        homeList.add("Boys")
        homeList.add("Fitness")
        homeList.add("Attitude")
        homeList.add("Attitude")
    }

}