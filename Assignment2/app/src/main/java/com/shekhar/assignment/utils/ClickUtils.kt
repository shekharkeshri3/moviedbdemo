package com.shekhar.assignment.utils

import android.view.View

object ClickUtils {

    /**
     * for click listner
     */
    fun setClickListener(views: Array<View>, listener: View.OnClickListener) {

        for (view in views) {
            view.setOnClickListener(listener)
            ClickGuard.guard(view)
        }
    }
}
