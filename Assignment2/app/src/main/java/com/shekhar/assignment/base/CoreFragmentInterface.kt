package  com.shekhar.assignment.base

import android.view.View


interface CoreFragmentInterface {
    fun layoutRes(): Int
    fun setValues()
    fun registerClickListener():Array<View>?
    fun getIntentValues()
}