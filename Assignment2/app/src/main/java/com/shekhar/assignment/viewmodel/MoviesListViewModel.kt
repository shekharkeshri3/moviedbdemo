package com.shekhar.assignment.viewmodel

import android.util.Log
import com.shekhar.assignment.viewmodel.repository.MovieRepository
import com.shekhar.assignment.viewmodel.repository.data.MovieList
import io.reactivex.Observable

class MoviesListViewModel(private val moviesRepository: MovieRepository) {


    fun getMovies(): Observable<MovieList> {
//        loading.value=true
        //Create the data for your UI, the users lists and maybe some additional data needed as well
        return moviesRepository.getMovies()
                .map {
//                    loading.value=false
                    Log.d("Mapping","Mapping users to UIData...")
                    MovieList( it.results)
                }
                .onErrorReturn {
//                    loading.value=false
                    Log.d("Mapping","An error occurred $it")
                    MovieList(emptyList())
                }
    }

}
