package com.shekhar.assignment.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.shekhar.assignment.R
import com.shekhar.assignment.view.fragment.RecyclerClick
import kotlinx.android.synthetic.main.caption_item_layout.view.*

class HomeListAdapter(private val mContext: Context?,
                      private var list: ArrayList<String>) : RecyclerView.Adapter<HomeListAdapter.RecyclerViewHolder>() {


    private lateinit var mOnItemClickCallback: RecyclerClick
    private var checkedPosition = 0

    public fun setOnItemClickListener(mOnItemClickCallback: RecyclerClick) {
        this.mOnItemClickCallback = mOnItemClickCallback

    }
    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.caption_item_layout, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindPerformersChild(list.get(position),position)
    }

    fun setUserList(list: ArrayList<String>){
        this.list=list
        notifyDataSetChanged()
    }
    inner class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindPerformersChild(movieGenresList: String,pos:Int) {
            itemView.caption_name_tv.text= movieGenresList

            /*if (pos==0){
                itemView.caption_main_cl.background = mContext?.let { ContextCompat.getDrawable(it, R.drawable.round_box_bg) }
                itemView.caption_name_tv.background = mContext?.let { ContextCompat.getDrawable(it, R.drawable.round_btn_txt_bg) }
            }else{
                itemView.caption_main_cl.background = mContext?.let { ContextCompat.getDrawable(it, R.drawable.round_btn_txt_bg) }
                itemView.caption_name_tv.background = mContext?.let { ContextCompat.getDrawable(it, R.drawable.round_box_gray_bg) }
            }*/

            if (checkedPosition == getAdapterPosition()) {
                itemView.caption_main_cl.background = mContext?.let { ContextCompat.getDrawable(it, R.drawable.round_box_bg) }
                itemView.caption_name_tv.background = mContext?.let { ContextCompat.getDrawable(it, R.drawable.round_btn_txt_bg) }
//                imageView.setVisibility(View.VISIBLE);
            } else {
//                imageView.setVisibility(View.GONE);
                itemView.caption_main_cl.background = mContext?.let { ContextCompat.getDrawable(it, R.drawable.round_btn_txt_bg) }
                itemView.caption_name_tv.background = mContext?.let { ContextCompat.getDrawable(it, R.drawable.round_box_gray_bg) }
            }
            itemView.itemCardView.setOnClickListener {
                if (checkedPosition != getAdapterPosition()) {
                    notifyItemChanged(checkedPosition)
                    checkedPosition = getAdapterPosition()
                    notifyItemChanged(checkedPosition)
                }
                mOnItemClickCallback.onItemClick(itemView.itemCardView, pos)
            }
        }
    }
}