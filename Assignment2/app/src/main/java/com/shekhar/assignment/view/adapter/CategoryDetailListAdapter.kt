package com.shekhar.assignment.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shekhar.assignment.R
import com.shekhar.assignment.view.fragment.RecyclerClick
import kotlinx.android.synthetic.main.category_detail_item.view.*
import kotlinx.android.synthetic.main.home_detail_layout.*

class CategoryDetailListAdapter(private val mContext: Context?,
                                private var list: ArrayList<String>) : RecyclerView.Adapter<CategoryDetailListAdapter.RecyclerViewHolder>() {


    private lateinit var mOnItemClickCallback: RecyclerClick

    var categorySubListItemAdapter:CategorySubListItemAdapter?=null
    private var subCatList = ArrayList<String>()

    public fun setOnItemClickListener(mOnItemClickCallback: RecyclerClick) {
        this.mOnItemClickCallback = mOnItemClickCallback

    }
    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.category_detail_item, parent, false)
        addList()
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindPerformersChild(list.get(position),position)
    }

    fun setUserList(list: ArrayList<String>){
        this.list=list
        notifyDataSetChanged()
    }
    inner class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindPerformersChild(catName: String,pos:Int) {
            categorySubListItemAdapter = CategorySubListItemAdapter(mContext, subCatList)
            itemView.hash_tag_cat_rcv.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
                adapter = this@CategoryDetailListAdapter.categorySubListItemAdapter
            }
        }
    }

    fun addList(){
        subCatList.clear()
        subCatList.add("see")
        subCatList.add("attitude")
        subCatList.add("positive")
        subCatList.add("quick")
        subCatList.add("judge")
        subCatList.add("travel")
        subCatList.add("pets")
        subCatList.add("food")
    }
}