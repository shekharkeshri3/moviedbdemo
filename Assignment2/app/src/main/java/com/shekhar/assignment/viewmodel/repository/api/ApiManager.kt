package com.shekhar.assignment.viewmodel.repository.api

import com.shekhar.assignment.viewmodel.repository.data.MovieDetailData
import com.shekhar.assignment.viewmodel.repository.data.MovieList
import com.shekhar.assignment.viewmodel.repository.data.PopularMovieList
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiManager {

    @GET("now_playing?language=en-US&page=undefined&api_key=55957fcf3ba81b137f8fc01ac5a31fb5")
    fun getMoviesList(): Observable<MovieList>

    @GET("popular?api_key=55957fcf3ba81b137f8fc01ac5a31fb5&language=en-US")
    fun getPopularMoviesList(@Query("page") page_num: String): Observable<PopularMovieList>


    @GET("{MOVIE_ID}?api_key=55957fcf3ba81b137f8fc01ac5a31fb5")
    fun getMoviesDetail(@Path("MOVIE_ID") page_num: String): Observable<MovieDetailData>

}
