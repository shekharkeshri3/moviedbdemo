package com.shekhar.assignment.viewmodel.repository.data

data class MovieDetailData(var poster_path:String,var title:String,var overview:String,var release_date:String,var genres: List<MovieGenresList>)