package com.shekhar.assignment.utils

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.widget.EditText
import androidx.appcompat.widget.AppCompatEditText
import com.shekhar.assignment.R
import com.shekhar.assignment.base.BaseApp


class CustomEditText : AppCompatEditText {

    private var typefaceType: Int = 0
    var mcontext:Context?=null
    val baseApp = BaseApp.injectBaseApp()

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        mcontext=context
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mcontext=context
        init(attrs)
    }

    constructor(context: Context) : super(context) {
        mcontext=context
        init(null)
    }

    private fun init(attrs: AttributeSet?) {
        if (attrs != null) {

            val array = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0
            )
            try {
                typefaceType = array.getInteger(R.styleable.CustomTextView_fontName, 0)
            } finally {
                array.recycle()
            }
            if (!isInEditMode) {
                typeface = mcontext?.let { baseApp.getTypeFace(typefaceType, it) };
            }
        }
    }
}
