package com.shekhar.assignment.viewmodel.repository.data

data class PopularMovieListItem(var poster_path: String?,var title: String?,var vote_average: String?,var release_date: String?,var id: Int?)