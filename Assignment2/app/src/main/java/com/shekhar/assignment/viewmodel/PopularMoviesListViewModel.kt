package com.shekhar.assignment.viewmodel

import android.util.Log
import com.shekhar.assignment.viewmodel.repository.PopularMovieRepository
import com.shekhar.assignment.viewmodel.repository.data.PopularMovieList
import io.reactivex.Observable

class PopularMoviesListViewModel(private val popularMovieRepository: PopularMovieRepository) {


    fun getPopularMovies(page_num:Int): Observable<PopularMovieList> {
//        loading.value=true
        //Create the data for your UI, the users lists and maybe some additional data needed as well
        return popularMovieRepository.getPoppularMovies(page_num)
                .map {
//                    loading.value=false
                    Log.d("Mapping","Mapping users to UIData...")
                    PopularMovieList( it.results)
                }
                .onErrorReturn {
//                    loading.value=false
                    Log.d("Mapping","An error occurred $it")
                    PopularMovieList(emptyList())
                }
    }

}
