package com.shekhar.assignment.utils

import android.content.Context
import android.util.AttributeSet
import com.shekhar.assignment.R
import com.shekhar.assignment.base.BaseApp
import androidx.appcompat.widget.AppCompatTextView


class CustomTextView : AppCompatTextView {

    private var typefaceType: Int = 0
    val baseApp = BaseApp.injectBaseApp()
    var mcontext:Context?=null

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        mcontext=context
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mcontext=context
        init(attrs)

    }

    constructor(context: Context) : super(context) {
        mcontext=context
        init(null)
    }

    private fun init(attrs: AttributeSet?) {
        if (isInEditMode) {
            return
        }
        if (attrs != null) {

            val array = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0
            )
            try {
                typefaceType = array.getInteger(R.styleable.CustomTextView_fontName, 0)
            } finally {
                array.recycle()
            }
            if (!isInEditMode) {
                typeface = mcontext?.let { baseApp.getTypeFace(typefaceType, it) }

            }
        }
    }

}
