package com.shekhar.assignment.base

import android.app.Application
import android.content.Context
import android.graphics.Typeface
import com.shekhar.assignment.utils.TypeFactory
import com.shekhar.assignment.viewmodel.MoviesDetailViewModel
import com.shekhar.assignment.viewmodel.MoviesListViewModel
import com.shekhar.assignment.viewmodel.PopularMoviesListViewModel
import com.shekhar.assignment.viewmodel.repository.MovieDetailRepository
import com.shekhar.assignment.viewmodel.repository.MovieRepository
import com.shekhar.assignment.viewmodel.repository.PopularMovieRepository
import com.shekhar.assignment.viewmodel.repository.api.ApiManager
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class BaseApp : Application() {

    //For the sake of simplicity, for now we use this instead of Dagger

    private var typeFactory: TypeFactory?=null

    companion object {



        private lateinit var baseApp: BaseApp
        private lateinit var retrofit: Retrofit
        private lateinit var apiManager: ApiManager
        private lateinit var movieRepository: MovieRepository
        private lateinit var moviesListViewModel: MoviesListViewModel

        private lateinit var popularMovieRepository: PopularMovieRepository
        private lateinit var popularMoviesListViewModel: PopularMoviesListViewModel

        private lateinit var movieDetailRepository: MovieDetailRepository
        private lateinit var moviesDetailViewModel: MoviesDetailViewModel
        private val BASE_URL = "https://api.themoviedb.org/3/movie/"
        fun injectUserApi() = apiManager
        fun injectMovieListViewModel() = moviesListViewModel
        fun injectPopularMovieListViewModel() = popularMoviesListViewModel
        fun injectPopularmoviesDetailViewModel() = moviesDetailViewModel
        fun injectBaseApp() = baseApp


    }

    override fun onCreate() {
        super.onCreate()
        retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BASE_URL)
            .build()

        apiManager = retrofit.create(ApiManager::class.java)
        baseApp = BaseApp()

        movieRepository = MovieRepository(apiManager)
        moviesListViewModel = MoviesListViewModel(movieRepository)

        popularMovieRepository = PopularMovieRepository(apiManager)
        popularMoviesListViewModel = PopularMoviesListViewModel(popularMovieRepository)

        movieDetailRepository = MovieDetailRepository(apiManager)
        moviesDetailViewModel = MoviesDetailViewModel(movieDetailRepository)

//        typeFactory = TypeFactory(this)
    }

    fun getTypeFace(type: Int,mContext:Context): Typeface? {
        if (typeFactory == null)
            typeFactory = TypeFactory(mContext)

        when (type) {
            Constants.REGULAR -> return typeFactory?.regular


            Constants.SEMIBOLD -> return typeFactory?.semiBold

            else -> return typeFactory?.regular
        }
    }


    interface Constants {
        companion object {
            val REGULAR = 1
            val SEMIBOLD = 2
        }

    }


}
