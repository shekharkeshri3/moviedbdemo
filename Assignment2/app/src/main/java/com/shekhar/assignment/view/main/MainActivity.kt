package com.shekhar.assignment.view.main

import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.shekhar.assignment.R
import com.shekhar.assignment.base.BaseActivity
import com.shekhar.assignment.base.FragmentManagerUtils
import com.shekhar.assignment.view.fragment.HomeFragment
import com.shekhar.assignment.view.fragment.MovieListFragment

class MainActivity : BaseActivity() {

    private var mFragmentManager: FragmentManager? = null

    override fun setCoordinatorLayout(): View? {
        return null
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_main
    }

    override fun setValues() {
        mFragmentManager = supportFragmentManager
//        replaceFragment(HomeFragment())
        replaceFragment(MovieListFragment())
    }

    override fun registerClickListener(): Array<View>? {
        return null
    }

    override fun setToolbarId(): View? {
        return null
    }


    /*--------------------------------manage fragments---------------------------------------*/
    fun addFragment(fragment: Fragment, mContainerId: Int = R.id.screenContainer) {
        FragmentManagerUtils.addFragment(
            mFragmentManager as FragmentManager,
            mContainerId,
            fragment
        )
    }

    fun replaceFragment(fragment: Fragment, mContainerId: Int = R.id.screenContainer) {
        FragmentManagerUtils.replaceFragment(
            mFragmentManager as FragmentManager,
            mContainerId,
            fragment
        )
    }
}
