package com.shekhar.assignment.view.fragment

import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shekhar.assignment.R
import com.shekhar.assignment.base.BaseApp
import com.shekhar.assignment.base.BaseFragment
import com.shekhar.assignment.view.adapter.MovieListAdapter
import com.shekhar.assignment.view.adapter.PopularMovieListAdapter
import com.shekhar.assignment.view.main.MainActivity
import com.shekhar.assignment.viewmodel.repository.data.MovieList
import com.shekhar.assignment.viewmodel.repository.data.MovieListItem
import com.shekhar.assignment.viewmodel.repository.data.PopularMovieList
import com.shekhar.assignment.viewmodel.repository.data.PopularMovieListItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.home_header_layout.*
import kotlinx.android.synthetic.main.moview_list_layout.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

class MovieListFragment : BaseFragment(), RecyclerClick {

    val moviesListViewModel = BaseApp.injectMovieListViewModel()
    val popularMoviesListViewModel = BaseApp.injectPopularMovieListViewModel()
    private lateinit var movieListAdapter: MovieListAdapter
    private lateinit var popularMovieListAdapter: PopularMovieListAdapter
    private var movieList = ArrayList<MovieListItem>()
    private var popularMovieList = ArrayList<PopularMovieListItem>()
    private var pageNum = 1
    private var isLoading = false
    private var mLinearLayoutManager: LinearLayoutManager? = null

    override fun layoutRes(): Int {
        return R.layout.moview_list_layout
    }

    override fun setValues() {
        movieListAdapter = MovieListAdapter(context, movieList)
        popularMovieListAdapter = PopularMovieListAdapter(context, popularMovieList)
        movieListRv.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = this@MovieListFragment.movieListAdapter
        }
        popular_movie_list_rv.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            mLinearLayoutManager = layoutManager as LinearLayoutManager
            adapter = this@MovieListFragment.popularMovieListAdapter
        }

        popularMovieListAdapter?.setOnItemClickListener(this)
        headerName.text="MOVIEBOX"
        progressbar!!.visibility = View.VISIBLE
        getMoviesList()
       /* suspend {

            coroutineScope {
                withContext(Dispatchers.IO) {
                    getPopularMovies(pageNum)
                }
            }

        }*/

        getPopularMovies(pageNum)




        popular_movie_list_rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                Log.e("dyy", "" + dy)
                if (!isLoading) {
                    if (dy > 0) //check for scroll down
                    {
//                        Log.e("dyy",""+dy)
                        val visibleItemCount = mLinearLayoutManager?.childCount
                        val totalItemCount = mLinearLayoutManager?.itemCount
                        val firstVisibleItemPosition = mLinearLayoutManager?.findFirstVisibleItemPosition()

//                        Log.e("dyy",""+dy)
//                        Log.e("visibleItemCount",""+visibleItemCount)
//                        Log.e("totalItemCount",""+totalItemCount)
//                        Log.e("totalItemCountwww",""+visibleItemCount?.plus(firstVisibleItemPosition!!))

                        if ((visibleItemCount?.plus(firstVisibleItemPosition!!))!! >= totalItemCount!!) {
                            isLoading = true
                            //call api with updated page number
                            pageNum += 1
                            Log.e("pagenum", "" + pageNum)
                           /* suspend {
                                withContext(Dispatchers.IO) {
                                    getPopularMovies(pageNum)
                                }
                            }*/
                            Handler().postDelayed({ getPopularMovies(pageNum)  }, 1000)
                        }
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }

     fun getMoviesList(){
        subscribe(moviesListViewModel.getMovies()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    progressbar!!.visibility = View.GONE
                    showMovies(it)

                }, {
                    progressbar!!.visibility = View.GONE
                    showError()
                })
        )
    }
    override fun onStart() {
        super.onStart()

    }

    override fun registerClickListener(): Array<View>? {
        return null
    }

    override fun onClick(p0: View?) {}

   fun getPopularMovies(page_num: Int) {
        subscribe(popularMoviesListViewModel.getPopularMovies(page_num)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showPopularMovies(it)
            }, {
                showError()
            })
        )
    }

    fun showMovies(data: MovieList) {
        if (data.results.size > 0) {
            movieListAdapter.setUserList(data.results as ArrayList<MovieListItem>)

        } else {
            showError()
        }
    }

   fun showPopularMovies(data: PopularMovieList) {

        if (data.results.size > 0) {
            isLoading = false
            popularMovieListAdapter.setUserList(data.results as ArrayList<PopularMovieListItem>)
        } else {
            showError()
        }
    }

    fun showError() {
        Toast.makeText(context, "An error occurred :(", Toast.LENGTH_SHORT).show()
    }

    override fun onItemClick(view: View, position: Int) {
        when (view?.id) {
            R.id.mainCl -> {
                popularMovieList = popularMovieListAdapter.getList()
                (activity as MainActivity).addFragment(MovieDetailFragment.getInstance(popularMovieList.get(position).id))
            }
        }
    }
}