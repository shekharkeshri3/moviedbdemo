package com.shekhar.assignment.viewmodel.repository.data


data class MovieList(var results: List<MovieListItem>)