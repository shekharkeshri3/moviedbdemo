package com.shekhar.assignment.viewmodel

import android.util.Log
import com.shekhar.assignment.viewmodel.repository.MovieDetailRepository
import com.shekhar.assignment.viewmodel.repository.data.MovieDetailData
import io.reactivex.Observable

class MoviesDetailViewModel(private val movieDetailRepository: MovieDetailRepository) {

    fun getMovieDetail(movie_id:Int): Observable<MovieDetailData> {
        //Create the data for your UI, the users lists and maybe some additional data needed as well
        return movieDetailRepository.getMovieDetail(movie_id)
                .map {
//                    loading.value=false
                    Log.d("Mapping","Mapping users to UIData...")
                    MovieDetailData( it.poster_path,it.title,it.overview,it.release_date,it.genres)
                }
                .onErrorReturn {
//                    loading.value=false
                    Log.d("Mapping","An error occurred $it")
                    MovieDetailData("","","","", emptyList())
                }
    }

}
