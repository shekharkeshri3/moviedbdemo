package com.shekhar.assignment.viewmodel.repository

import android.annotation.SuppressLint
import android.util.Log
import com.shekhar.assignment.viewmodel.repository.api.ApiManager
import com.shekhar.assignment.viewmodel.repository.data.MovieList
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class MovieRepository(val apiManager: ApiManager) {

    fun getMovies(): Observable<MovieList> {
        return Observable.concatArray(
            getMoviesFromApi()
        )
//        return apiManager.getMoviesList().subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
    }

    fun getMoviesFromApi(): Observable<MovieList> {
        return apiManager.getMoviesList()
            .doOnNext {
                Log.d("Dispatching", "Dispatching ${it.results.size} users from API...")
                storeUsersInDb(it)
            }.cache()
    }

    @SuppressLint("CheckResult")
    fun storeUsersInDb(movieList: MovieList) {
        Observable.fromCallable { }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe {
                Log.d("Inserted", "Inserted ${movieList.results.size} users from API in DB...")
            }
    }

}
