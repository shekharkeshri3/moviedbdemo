package com.shekhar.assignment.view.fragment

import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shekhar.assignment.R
import com.shekhar.assignment.base.BaseFragment
import com.shekhar.assignment.view.adapter.CategoryDetailListAdapter
import com.shekhar.assignment.view.adapter.CategoryListAdapter
import kotlinx.android.synthetic.main.home_detail_layout.*

class HomeDetailFragment: BaseFragment(), RecyclerClick{

    private var homeList = ArrayList<String>()
    private lateinit var categoryListAdapter: CategoryListAdapter
    private lateinit var categoryDetailListAdapter: CategoryDetailListAdapter


    companion object {
        private lateinit var mInstance: HomeDetailFragment
        fun getInstance(): HomeDetailFragment {
            mInstance = HomeDetailFragment()
            return mInstance
        }

        /*fun getInstance(
            vendorId: String,
            vendorName: String,
            vendorMobNum: String,
            vendorComp: String,
            visitorProfilePic: String
        ): HomeDetailFragment {
            val bundle = Bundle()
            bundle.putString("vendorId", vendorId)
            bundle.putString("vendorName", vendorName)
            bundle.putString("vendorMobNum", vendorMobNum)
            bundle.putString("vendorComp", vendorComp)
            bundle.putString("visitorProfilePic", visitorProfilePic)
            mInstance = HomeDetailFragment()
            mInstance.arguments = bundle
            return mInstance
        }*/
    }

    override fun layoutRes(): Int {
        return R.layout.home_detail_layout
    }

    override fun setValues() {
        addList()
        categoryListAdapter = CategoryListAdapter(context, homeList)
        categoryDetailListAdapter = CategoryDetailListAdapter(context, homeList)
        home_cat_rcv.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = this@HomeDetailFragment.categoryListAdapter
        }
        home_cat_detail_rcv.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = this@HomeDetailFragment.categoryDetailListAdapter
        }
        categoryListAdapter.setOnItemClickListener(this)
    }

    override fun onItemClick(view: View, position: Int) {
        when(view.id){
            R.id.cat_name_tv -> {
                Toast.makeText(mContext,homeList.get(position),Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun registerClickListener(): Array<View>? {
        return null
    }

    override fun onClick(v: View?) {
    }
    fun addList(){
        homeList.clear()
        homeList.add("Attitude")
        homeList.add("Beauty")
        homeList.add("Cofee")
        homeList.add("Art")
        homeList.add("Books")
        homeList.add("Birthday")
        homeList.add("Boys")
        homeList.add("Fitness")
        homeList.add("Attitude")
        homeList.add("Attitude")
    }

}