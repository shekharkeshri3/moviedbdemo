package com.shekhar.assignment.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.shekhar.assignment.R
import com.shekhar.assignment.utils.Constants
import com.shekhar.assignment.view.fragment.RecyclerClick
import com.shekhar.assignment.viewmodel.repository.data.PopularMovieListItem
import kotlinx.android.synthetic.main.movie_item.view.*

class PopularMovieListAdapter(private val mContext: Context?,
                              private var list: ArrayList<PopularMovieListItem>) : RecyclerView.Adapter<PopularMovieListAdapter.RecyclerViewHolder>() {


    private lateinit var mOnItemClickCallback: RecyclerClick

    public fun setOnItemClickListener(mOnItemClickCallback: RecyclerClick) {
        this.mOnItemClickCallback = mOnItemClickCallback

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.movie_item, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindPerformersChild(list.get(position),position)
    }

    fun setUserList(list: ArrayList<PopularMovieListItem>){
        if(this.list.size>0){
            this.list.addAll(list)
        }else{
            this.list=list
        }
        Log.e("listsize",""+list.size)
        Log.e("addedlistsize",""+this.list.size)
        notifyDataSetChanged()
    }
    fun getList(): ArrayList<PopularMovieListItem> {
        return list
    }

    inner class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindPerformersChild(movieListItem: PopularMovieListItem,position: Int) {
            val requestOptions = RequestOptions()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
            Glide.with(mContext!!)
                .setDefaultRequestOptions(requestOptions)
                .load(Constants.IMAGE_URL+movieListItem.poster_path)
                .into(itemView.thumbImg)

            itemView.titleTv.text=movieListItem.title
            itemView.dateTv.text= movieListItem.release_date?.let { Constants.dateConverter(it) }
            itemView.rateTv.text="Rating : ${movieListItem.vote_average}"

            itemView.mainCl.setOnClickListener {
                mOnItemClickCallback.onItemClick(itemView.mainCl, position)
            }
        }
    }
}