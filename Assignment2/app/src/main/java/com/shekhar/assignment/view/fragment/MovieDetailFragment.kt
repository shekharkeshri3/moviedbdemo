package com.shekhar.assignment.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.shekhar.assignment.R
import com.shekhar.assignment.base.BaseApp
import com.shekhar.assignment.base.BaseFragment
import com.shekhar.assignment.utils.Constants
import com.shekhar.assignment.view.adapter.GenresListAdapter
import com.shekhar.assignment.viewmodel.repository.data.MovieDetailData
import com.shekhar.assignment.viewmodel.repository.data.MovieGenresList
import com.shekhar.assignment.viewmodel.repository.data.PopularMovieList
import com.shekhar.assignment.viewmodel.repository.data.PopularMovieListItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.movie_detail_layout.*
import kotlinx.android.synthetic.main.movie_item.view.*

class MovieDetailFragment:BaseFragment(){

    val moviesDetailViewModel = BaseApp.injectPopularmoviesDetailViewModel()
    var movie_id:Int?=0
    private  var movieGenresList=ArrayList<MovieGenresList>()
    private lateinit var genresListAdapter: GenresListAdapter
    override fun layoutRes(): Int {
        return R.layout.movie_detail_layout
    }
    companion object {
        private lateinit var mInstance: MovieDetailFragment
        fun getInstance(movie_id: Int?): MovieDetailFragment {
            val bundle = Bundle()
            movie_id?.let { bundle.putInt("movie_id", it) }
            mInstance = MovieDetailFragment()
            mInstance.arguments = bundle
            return mInstance
        }
    }
    override fun setValues() {
        genresListAdapter = GenresListAdapter(context , movieGenresList)
        genresRv.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = this@MovieDetailFragment.genresListAdapter
        }
        movie_id?.let { getMovieDetail(it) }
        progressbar!!.visibility = View.VISIBLE
    }

    override fun registerClickListener(): Array<View>? {
        return arrayOf(btnBack)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnBack->{activity?.onBackPressed()}
        }
    }

    override fun getIntentValues() {
        super.getIntentValues()
        val argument = arguments
        movie_id = argument?.getInt("movie_id")
        Log.e("bookingId", "" + movie_id)
    }
    fun getMovieDetail(movie_id:Int){
        subscribe(moviesDetailViewModel.getMovieDetail(movie_id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                progressbar!!.visibility = View.GONE
                showMoviewDetail(it)
            }, {
                progressbar!!.visibility = View.GONE
                showError()
            }))
    }

    fun showMoviewDetail(data: MovieDetailData) {
        if(!data.release_date.isEmpty()){
            titleTv.text=data.title
            overviewDetailTv.text=data.overview
            datevTv.text=data.release_date?.let { Constants.dateConverter(it) }

            val requestOptions = RequestOptions()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
            Glide.with(mContext!!)
                .setDefaultRequestOptions(requestOptions)
                .load(Constants.IMAGE_URL+data.poster_path)
                .into(posterImg)

            genresListAdapter.setUserList(data.genres as ArrayList<MovieGenresList>)
        }else{
            showError()
        }

    }

    fun showError() {
        Toast.makeText(context, "An error occurred :(", Toast.LENGTH_SHORT).show()
    }

}