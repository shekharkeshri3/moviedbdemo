package com.shekhar.assignment.utils

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat

class Constants{
    companion object {
        val IMAGE_URL="https://image.tmdb.org/t/p/original"

        @SuppressLint("SimpleDateFormat")
        fun dateConverter(dateStr: String): String {
            var s = ""
            try {
                val format1 = SimpleDateFormat("yyyy-MM-dd")
                val format2 = SimpleDateFormat("MMM dd, yyyy")
                val date = format1.parse(dateStr)
                s = format2.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return s
        }
    }
}