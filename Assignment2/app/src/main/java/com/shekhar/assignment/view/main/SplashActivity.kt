package com.shekhar.assignment.view.main

import android.content.Intent
import android.view.View
import com.shekhar.assignment.R
import com.shekhar.assignment.base.BaseActivity
import kotlinx.android.synthetic.main.layout_splash.*
import kotlin.system.exitProcess

class SplashActivity: BaseActivity(){

    /**
     * Called when the activity is first created.
     */
    private lateinit var splashTread: Thread
    var tokenStr=""

    override fun setCoordinatorLayout(): View? {
        return splash
    }

    override fun setLayoutResource(): Int {
        return R.layout.layout_splash
    }

    override fun setValues() {
        getDeviceId()
    }

    override fun registerClickListener(): Array<View>? {
        return null
    }

    override fun setToolbarId(): View? {
       return null
    }
    private fun getDeviceId() {
       /* val android_id = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        if(android_id!=null && !android_id.isEmpty()){
            UserPreference.getInstance(mContext).deviceId = android_id
            Log.d("Gloabla Method Token111", UserPreference.getInstance(mContext).deviceRegistrationId)

            getDeviceToken()*/
            splashTimer()
        }


//    fun getDeviceToken(): String {
//
//        FirebaseInstanceId.getInstance().instanceId
//            .addOnCompleteListener(OnCompleteListener { task ->
//                if (!task.isSuccessful) {
//                    //                            Log.w(TAG, "getInstanceId failed", task.getException());
//                    return@OnCompleteListener
//                }
//
//                // Get new Instance ID token
//                val token = task.result!!.token
//                 tokenStr = token
//
//                if(UserPreference.getInstance(mContext).deviceRegistrationId.isEmpty()){
//                    UserPreference.getInstance(mContext).setDeviceRegistrationID(tokenStr)
//                }
//                splashTimer()
//                // Log and toast
//                //                        String msg = getString(R.string.msg_token_fmt, token);
//                Log.d("Gloabla Method Token", tokenStr)
//                //                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
//            })
//
//        return tokenStr
//    }


    private fun splashTimer() {
        splashTread = object : Thread() {
            override fun run() {
                try {
                    var waited = 0
                    // Splash screen svg_play time
                    while (waited < 1500) {
                        sleep(100)
                        waited += 100
                    }
                    startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                    this@SplashActivity.finish()


                } catch (e: InterruptedException) {
                //   Log.e("message", e.toString())
                } finally {
                    this@SplashActivity.finish()
                }
            }
        }
        splashTread.start()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        exitProcess(0)
    }
}
