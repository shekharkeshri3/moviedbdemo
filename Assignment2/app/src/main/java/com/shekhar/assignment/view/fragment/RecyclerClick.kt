package com.shekhar.assignment.view.fragment

import android.view.View

interface RecyclerClick {
   fun onItemClick(view:View,position:Int)
}