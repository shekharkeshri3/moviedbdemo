package com.shekhar.assignment.viewmodel.repository

import android.annotation.SuppressLint
import com.shekhar.assignment.viewmodel.repository.api.ApiManager
import com.shekhar.assignment.viewmodel.repository.data.MovieDetailData
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class MovieDetailRepository(val apiManager: ApiManager) {

    fun getMovieDetail(page_num:Int): Observable<MovieDetailData> {
        return Observable.concatArray(
            getMovieDetailFromApi(page_num)
        )
    }

    fun getMovieDetailFromApi(movie_id:Int): Observable<MovieDetailData> {
        return apiManager.getMoviesDetail(movie_id.toString())
            .doOnNext {
//                Log.d("Dispatching", "Dispatching ${it.results.size} users from API...")
                storeUsersInDb(it)
            }
    }

    @SuppressLint("CheckResult")
    fun storeUsersInDb(movieList: MovieDetailData) {
        Observable.fromCallable { }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe {
//                Log.d("Inserted", "Inserted ${movieList.results.size} users from API in DB...")
            }
    }

}
