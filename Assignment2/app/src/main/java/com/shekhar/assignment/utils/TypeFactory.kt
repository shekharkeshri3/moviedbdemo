package com.shekhar.assignment.utils

import android.content.Context
import android.graphics.Typeface

/**
 * Created by Shekhar Kumar.
 */
class TypeFactory(context: Context) {

    internal val OPEN_SENS_REGULAR = "fonts/OpenSans-Regular_0.ttf"
    internal val OPEN_SANS_SEMI_BOLD = "fonts/OpenSans-Semibold_0.ttf"


    var regular: Typeface
        internal set
    var semiBold: Typeface
        internal set

    init {
        regular = Typeface.createFromAsset(context.assets, OPEN_SENS_REGULAR)
        semiBold = Typeface.createFromAsset(context.assets, OPEN_SANS_SEMI_BOLD)
    }

}
