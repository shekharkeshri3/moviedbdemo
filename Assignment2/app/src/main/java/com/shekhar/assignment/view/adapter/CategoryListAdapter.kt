package com.shekhar.assignment.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.shekhar.assignment.R
import com.shekhar.assignment.view.fragment.RecyclerClick
import kotlinx.android.synthetic.main.caption_item_layout.view.*
import kotlinx.android.synthetic.main.category_item.view.*

class CategoryListAdapter(private val mContext: Context?,
                          private var list: ArrayList<String>) : RecyclerView.Adapter<CategoryListAdapter.RecyclerViewHolder>() {


    private lateinit var mOnItemClickCallback: RecyclerClick
    private var checkedPosition = 0

    public fun setOnItemClickListener(mOnItemClickCallback: RecyclerClick) {
        this.mOnItemClickCallback = mOnItemClickCallback

    }
    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.category_item, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindPerformersChild(list.get(position),position)
    }

    fun setUserList(list: ArrayList<String>){
        this.list=list
        notifyDataSetChanged()
    }
    inner class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindPerformersChild(catName: String,pos:Int) {
            itemView.cat_name_tv.text= catName
            if (checkedPosition == getAdapterPosition()) {
                itemView.cat_name_tv.background = mContext?.let { ContextCompat.getDrawable(it, R.drawable.round_box_bg) }
                mContext?.let { ContextCompat.getColor(it,R.color.white) }?.let { itemView.cat_name_tv.setTextColor(it) }
            }else{
                itemView.cat_name_tv.background = mContext?.let { ContextCompat.getDrawable(it, R.drawable.round_btn_txt_bg) }
                mContext?.let { ContextCompat.getColor(it,R.color.black) }?.let { itemView.cat_name_tv.setTextColor(it) }
            }

            itemView.cat_name_tv.setOnClickListener {
                if (checkedPosition != getAdapterPosition()) {
                    notifyItemChanged(checkedPosition)
                    checkedPosition = getAdapterPosition()
                    notifyItemChanged(checkedPosition)
                }
                mOnItemClickCallback.onItemClick(itemView.cat_name_tv, pos)
            }
        }
    }
}