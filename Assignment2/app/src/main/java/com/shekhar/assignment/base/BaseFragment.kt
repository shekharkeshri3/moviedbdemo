package com.shekhar.assignment.base

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.shekhar.assignment.utils.ClickUtils
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

  abstract class BaseFragment : Fragment(),CoreFragmentInterface,View.OnClickListener {
    private var activity: AppCompatActivity? = null


    val subscriptions = CompositeDisposable()

      var mActivity: FragmentActivity? = null
      var mContext: Context? = null
      var baseActivityInstance: BaseActivity? = null
      var mPackageManager: PackageManager? = null
    fun subscribe(disposable: Disposable): Disposable {
        subscriptions.add(disposable)
        return disposable
    }

      override fun onCreate(savedInstanceState: Bundle?) {
          super.onCreate(savedInstanceState)
          this.mContext=context
          this.mActivity=activity
          this.mPackageManager=mContext?.packageManager
          getIntentValues()
      }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var mLayoutId: Int = layoutRes()
        if (mLayoutId != -1) {
            return inflater?.inflate(mLayoutId, container, false)
        }

        return null
    }
      private var savedInstanceState: Bundle? = null
      fun getSavedInstanceState(): Bundle? {
          return savedInstanceState
      }

      override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
          super.onViewCreated(view, savedInstanceState)
          if (view != null) {
              this.savedInstanceState = savedInstanceState
              setValues()
              val viewList = registerClickListener();
              if (viewList != null && viewList.isNotEmpty()) {
                  ClickUtils.setClickListener(viewList, this)
              }
          }
      }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as AppCompatActivity?
    }

    override fun onDetach() {
        super.onDetach()
        activity = null
    }

    fun getBaseActivity(): AppCompatActivity? {
        return activity
    }

    override fun onStop() {
        super.onStop()
        subscriptions.clear()

    }

    override fun onDestroyView() {
//        subscriptions.dispose()
        super.onDestroyView()
    }

      override fun getIntentValues() {
      }

}