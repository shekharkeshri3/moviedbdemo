package com.shekhar.assignment.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.View

abstract class BaseActivity : BaseCoreActivity() {

    private var connectionFiler: IntentFilter? = null
    public var internetConnectionStatus: Boolean = false
    private var isScreenFirstTime: Boolean = false
    //        var coordinatorLytId: Int = -1
    private var CoordinatorLayout: View? = null;
    //    private var mPreferenceManager: PreferenceManager? = null
    abstract fun setCoordinatorLayout(): View?


    override fun onCreate(savedInstanceState: Bundle?) {
//        mPreferenceManager = PreferenceManager(mContext)
        super.onCreate(savedInstanceState)
        CoordinatorLayout = setCoordinatorLayout()
        initializeFilter()
        registerReceiver(mConnectionReceiver, connectionFiler)
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onPause() {
        super.onPause()

    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(mConnectionReceiver)
    }

    private val mConnectionReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action.equals(
                    ConnectivityManager.CONNECTIVITY_ACTION)) {

                val connectivityManager: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val ni: NetworkInfo? = connectivityManager.activeNetworkInfo
//                System.out.println("Internet... ")
                /*if (ni != null && ni.isAvailable && ni.isConnected) {
                    internetConnectionStatus = true
                    if (isScreenFirstTime) {
                        val snackbar = Snackbar
                                .make(CoordinatorLayout as CoordinatorLayout, "Internet is available", Snackbar.LENGTH_LONG)
                                .setActionTextColor(ResourceUtils.getColorFromResource(context, R.color.colorPrimaryDark))
//                                .setAction("RETRY") {
//                                    val snackbar1 = Snackbar.make(CoordinatorLayout, "Message is restored!", Snackbar.LENGTH_SHORT)
//
//                                    snackbar1.show()
//                                }
                        snackbar.show()
                    }
                    isScreenFirstTime = true
                } else {
//                    System.out.println("Internet " + false)
                    internetConnectionStatus = false
                    val snackbar = Snackbar
                            .make(CoordinatorLayout as CoordinatorLayout, "Internet not available", Snackbar.LENGTH_INDEFINITE)
                    snackbar.show()
                    isScreenFirstTime = true
                }*/


            }
        }
    }

    private fun initializeFilter() {
        connectionFiler = IntentFilter()
        connectionFiler!!.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
    }

    public fun isInternetAvailable(): Boolean {
        return internetConnectionStatus;
    }

    /*public fun getPreferenceManagerInstance(): PreferenceManager? {
        return mPreferenceManager
    }*/

    open fun onNewFragmentCreate(){

    }
}