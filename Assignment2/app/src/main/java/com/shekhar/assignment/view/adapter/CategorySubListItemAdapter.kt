package com.shekhar.assignment.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shekhar.assignment.R
import com.shekhar.assignment.viewmodel.repository.data.MovieGenresList
import kotlinx.android.synthetic.main.genres_layout_item.view.*

class CategorySubListItemAdapter(private val mContext: Context?,
                                 private var list: ArrayList<String>
) : RecyclerView.Adapter<CategorySubListItemAdapter.RecyclerViewHolder>() {

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.genres_layout_item, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindPerformersChild(list.get(position))
    }

    fun setUserList(list: ArrayList<String>){
        this.list=list
        notifyDataSetChanged()
    }
    inner class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindPerformersChild(listItem: String) {
            itemView.genres_name_tv.text= "#$listItem"
        }
    }
}