package  com.shekhar.assignment.base

import android.view.View


interface CoreActivityInterface {
    fun setLayoutResource(): Int
    fun setValues()
    fun registerClickListener(): Array<View>?
    fun setToolbarId(): View?
    fun getIntentValues()
}
