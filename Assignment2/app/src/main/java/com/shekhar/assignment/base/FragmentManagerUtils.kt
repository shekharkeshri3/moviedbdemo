package com.shekhar.assignment.base;

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager


class FragmentManagerUtils {
    companion object {

        fun replaceFragment(fragmentManager: FragmentManager, containerId: Int, fragment: Fragment) {
            val tag = fragment.javaClass.name;
            fragmentManager.beginTransaction()
                .replace(containerId, fragment,tag)
               // .addToBackStack(tag)
              //  .commitAllowingStateLoss()
                .commit()
        }

        fun addFragment(fragmentManager: FragmentManager, containerId: Int, fragment: Fragment) {
            val tag = fragment.javaClass.name;
            fragmentManager.beginTransaction()
                .add(containerId, fragment, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss()
        }
    }
}