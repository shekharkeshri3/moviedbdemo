package com.shekhar.assignment.viewmodel.repository.data


data class PopularMovieList(var results: List<PopularMovieListItem>)